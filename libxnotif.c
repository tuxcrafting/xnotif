#include "libxnotif.h"

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <unistd.h>

// Helpers

// determine the current X display number.
static int determine_display() {
	// get $DISPLAY.
	char *display_env = getenv("DISPLAY");
	if (display_env == NULL) {
		return -1;
	}

	// check it starts with ':'.
	if (display_env[0] != ':') {
		return -1;
	}

	// get the longest number at the start.
	size_t span = strspn(display_env + 1, "0123456789");

	// exit if there's no number.
	if (span == 0) {
		return -1;
	}

	// parse the number.
	int n = 0;
	for (size_t i = 0; i < span; i++) {
		n *= 10;
		n += display_env[i + 1] - '0'; // this may fail on non-ASCII platforms.
	}
	return n;
}

// initialize a socket address from the display number.
struct sockaddr_un *init_sockaddr(int display) {
	struct sockaddr_un *addr = malloc(sizeof(struct sockaddr_un));
	if (addr == NULL) {
		return NULL;
	}
	memset(addr, 0, sizeof(struct sockaddr_un));
	addr->sun_family = AF_UNIX;
	snprintf(addr->sun_path, sizeof(addr->sun_path) - 1, "/tmp/xnotif-%d.sock", display);
	return addr;
}

// check if file exists.
char file_exists(const char *path) {
	struct stat st;
	if (stat(path, &st) < 0) {
		return 0;
	}
	return 1;
}

// Misc

const char *xnotif_strerr(enum xnotif_error error) {
#define ENTRY(n, s) case n: return s
	switch (error) {
		ENTRY(XNOTIF_SUCCESS, "Success");
		ENTRY(XNOTIF_ERROR, "Unspecified error");
		ENTRY(XNOTIF_SOCK_OPEN_ERROR, "Could not open socket");
		ENTRY(XNOTIF_SOCK_OPEN, "Socket already open");
		ENTRY(XNOTIF_SOCK_ERROR, "Socket error");
		ENTRY(XNOTIF_INTERRUPTED, "Interrupt received");
		ENTRY(XNOTIF_INVALID_DGRAM, "Datagram is invalid");
		ENTRY(XNOTIF_INVALID_DISPLAY, "Invalid display number");
		ENTRY(XNOTIF_EMPTY_BODY, "Empty body");
	default:
		return "Unknown error";
	}
#undef ENTRY
}

// Server

// read buffer size.
#define BUFSIZE (64 * 1024)

enum xnotif_error xnotif_server_open(struct xnotif_server *server,
                                     int display) {
	// get display number.
	if (display == -1) {
		display = determine_display();
	}
	if (display < 0) {
		return XNOTIF_INVALID_DISPLAY;
	}

	// create address.
	struct sockaddr_un *addr = init_sockaddr(display);
	if (addr == NULL) {
		return XNOTIF_ERROR;
	}

	// check the file doesn't exist.
	if (file_exists(addr->sun_path)) {
		free(addr);
		return XNOTIF_SOCK_OPEN;
	}
	server->addr = addr;

	// create the socket.
	int fd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (fd < 0) {
		free(addr);
		return XNOTIF_SOCK_OPEN_ERROR;
	}
	server->fd = fd;

	// bind the socket.
	if (bind(fd,
	         (struct sockaddr*)addr,
	         sizeof(struct sockaddr_un)) < 0) {
		free(addr);
		close(fd);
		return XNOTIF_SOCK_OPEN_ERROR;
	}

	// create read buffer.
	server->readbuf = malloc(BUFSIZE);
	if (server->readbuf == NULL) {
		free(addr);
		close(fd);
		return XNOTIF_ERROR;
	}

	return XNOTIF_SUCCESS;
}

enum xnotif_error xnotif_server_recv(struct xnotif_server *server,
                                     struct xnotif_notification *notification) {
	// receive data.
	uint8_t *buf = server->readbuf;
	int read = recvfrom(server->fd, buf, BUFSIZE, 0, NULL, NULL);
	if (read < 0) {
		if (errno == EINTR) {
			return XNOTIF_INTERRUPTED;
		} else {
			return XNOTIF_SOCK_ERROR;
		}
	}

	// check length.
	if (read < 4) {
		return XNOTIF_INVALID_DGRAM;
	}

	// check version.
	if (buf[0] != 1) {
		return XNOTIF_INVALID_DGRAM;
	}

	// get urgency.
	uint8_t urgency = buf[1];
	if (urgency > 2) {
		return XNOTIF_INVALID_DGRAM;
	}
	notification->urgency = urgency;

	// get title.
	size_t t_base = 2;
	size_t t_len = 0;

	while (buf[t_base + t_len] != 0 && t_len < BUFSIZE - t_base) {
		t_len++;
	}

	if (t_len >= read - t_base) {
		return XNOTIF_INVALID_DGRAM;
	}

	// get body.
	size_t b_base = t_base + t_len + 1;
	size_t b_len = 0;

	while (buf[b_base + b_len] != 0 && b_len < read - b_base) {
		b_len++;
	}

	if (b_len == 0 || b_len >= read - b_base) {
		return XNOTIF_INVALID_DGRAM;
	}

	// set body and title.
	if (t_len == 0) {
		notification->title = NULL;
	} else {
		notification->title = (char*)buf + t_base;
	}
	notification->body = (char*)buf + b_base;

	return XNOTIF_SUCCESS;
}

enum xnotif_error xnotif_server_close(struct xnotif_server *server) {
	close(server->fd);

	// unlink socket.
	unlink(((struct sockaddr_un*)server->addr)->sun_path);

	free(server->addr);
	free(server->readbuf);
	return XNOTIF_SUCCESS;
}

// Client

enum xnotif_error xnotif_client_open(struct xnotif_client *client,
                                     int display) {
	// get display number.
	if (display == -1) {
		display = determine_display();
	}
	if (display < 0) {
		return XNOTIF_INVALID_DISPLAY;
	}

	// create address.
	struct sockaddr_un *addr = init_sockaddr(display);
	if (addr == NULL) {
		return XNOTIF_ERROR;
	}

	// check the file exists.
	if (!file_exists(addr->sun_path)) {
		free(addr);
		return XNOTIF_SOCK_OPEN_ERROR;
	}
	client->addr = addr;

	// create the socket.
	int fd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (fd < 0) {
		free(addr);
		return XNOTIF_SOCK_OPEN_ERROR;
	}
	client->fd = fd;

	return XNOTIF_SUCCESS;
}

enum xnotif_error xnotif_client_send(struct xnotif_client *client,
                                     enum xnotif_urgency urgency,
                                     char *title,
                                     char *body) {
	struct xnotif_notification notification;
	notification.urgency = urgency;
	notification.title = title;
	notification.body = body;
	return xnotif_client_send_raw(client, &notification);
}

enum xnotif_error xnotif_client_send_raw(struct xnotif_client *client,
                                         struct xnotif_notification *notification) {
	// get string lengths.
	size_t t_len = notification->title == NULL ? 0 : strlen(notification->title);
	size_t b_len = strlen(notification->body);

	if (b_len == 0) {
		return XNOTIF_EMPTY_BODY;
	}

	// allocate buffer and set version and urgency.
	size_t buf_len = 4 + t_len + b_len;
	uint8_t *buf = malloc(buf_len);
	if (buf == NULL) {
		return XNOTIF_ERROR;
	}
	buf[0] = 1;
	buf[1] = notification->urgency;

	// copy title.
	if (notification->title == NULL) {
		buf[2] = 0;
	} else {
		strcpy((char*)buf + 2, notification->title);
	}

	// copy body.
	strcpy((char*)buf + 3 + t_len, notification->body);

	// send.
	if (sendto(client->fd, buf, buf_len, 0, client->addr, sizeof(struct sockaddr_un)) < 0) {
		free(buf);
		return XNOTIF_SOCK_ERROR;
	}

	free(buf);

	return XNOTIF_SUCCESS;
}

enum xnotif_error xnotif_client_close(struct xnotif_client *client) {
	free(client->addr);
	close(client->fd);
	return XNOTIF_SUCCESS;
}
