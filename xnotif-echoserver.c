#include "libxnotif.h"

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct xnotif_server server;

static void print_usage(int argc, char **argv) {
	printf("Usage: %s [-h] [-d display]\n", argv[0]);
	printf("Simple XNotif server that outputs what it receives.\n");
	printf("\t-d\tSpecify display number.\n");
	printf("\t-h\tPrint this help message.\n");
}

static void sigint_handler(int sig) {
	exit(0);
}

static void exit_handler() {
	xnotif_server_close(&server);
}

int main(int argc, char *argv[]) {
	int display = -1;

	// parse options.
	int opt;
	while ((opt = getopt(argc, argv, "dh")) != -1) {
		switch (opt) {
		case 'd':
			if (optind == argc) {
				print_usage(argc, argv);
				return 1;
			}
			display = atoi(argv[optind++]);
			break;
		default:
			print_usage(argc, argv);
			return 1;
		}
	}

	// open server.
	enum xnotif_error err;
	if ((err = xnotif_server_open(&server, display)) != XNOTIF_SUCCESS) {
		fprintf(stderr, "xnotif_server_open() failed: %s\n", xnotif_strerr(err));
		return 1;
	}

	// set SIGINT to handler.
	struct sigaction act = { 0 };
	act.sa_handler = sigint_handler;
	sigaction(SIGINT, &act, NULL);
	atexit(exit_handler);

	// handle notifications.
	for (;;) {
		struct xnotif_notification notification;
		if ((err = xnotif_server_recv(&server, &notification)) != XNOTIF_SUCCESS) {
			fprintf(stderr, "xnotif_server_recv() failed: %s\n", xnotif_strerr(err));
			continue;
		}
		if (notification.title == NULL) {
			printf("%d (no title) '%s'\n",
			       notification.urgency, notification.body);
		} else {
			printf("%d '%s' '%s'\n",
			       notification.urgency, notification.title, notification.body);
		}
	}
}
