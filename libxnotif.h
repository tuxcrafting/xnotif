/**
 * @brief
 * @file LibXNotif is a library for interfacing with the XNotif protocol.
 */

#pragma once

#include <stdint.h>

// Structs

/// @brief XNotif server.
struct xnotif_server {
	/// @brief Socket file descriptor.
	int fd;

	/// @brief Socket address.
	void *addr;

	/// @brief Socket read buffer.
	uint8_t *readbuf;
};

/// @brief XNotif client.
struct xnotif_client {
	/// @brief Socket file descriptor.
	int fd;

	/// @brief Socket address.
	void *addr;
};

/// @brief XNotif error code.
enum xnotif_error {
	/// @brief Success.
	XNOTIF_SUCCESS = 0,

	/// @brief Unspecified error.
	XNOTIF_ERROR,

	/// @brief Could not open socket.
	XNOTIF_SOCK_OPEN_ERROR,

	/// @brief Socket already open.
	XNOTIF_SOCK_OPEN,

	/// @brief Socket error.
	XNOTIF_SOCK_ERROR,

	/// @brief Interrupt received.
	XNOTIF_INTERRUPTED,

	/// @brief Datagram is invalid.
	XNOTIF_INVALID_DGRAM,

	/// @brief Invalid display number.
	XNOTIF_INVALID_DISPLAY,

	/// @brief Empty body.
	XNOTIF_EMPTY_BODY,
};

/// @brief XNotif notification urgency.
enum xnotif_urgency {
	/// @brief Low.
	XNOTIF_URG_LOW = 0,

	/// @brief Medium.
	XNOTIF_URG_MEDIUM,

	/// @brief High.
	XNOTIF_URG_HIGH,
};

/// @brief XNotif notification.
struct xnotif_notification {
	/// @brief Notification urgency.
	enum xnotif_urgency urgency;

	/// @brief Notification title. NULL if empty.
	char *title;

	/// @brief Notification body.
	char *body;
};

// Misc

/**
 * @brief Get the string representation of an error.
 * @param error Error code.
 * @return String representation.
 */
const char *xnotif_strerr(enum xnotif_error error);

// Server

/**
 * @brief Open an XNotif server.
 * @param server Server object.
 * @param display Display number. -1 to automatically determine the display.
 * @return Error code.
 */
enum xnotif_error xnotif_server_open(struct xnotif_server *server,
                                     int display);

/**
 * @brief Wait for and receive a notification.
 * @param server Server object.
 * @param notification Notification object. Is valid until the next call to xnotif_server_recv() or xnotif_server_close().
 * @return Error code.
 */
enum xnotif_error xnotif_server_recv(struct xnotif_server *server,
                                     struct xnotif_notification *notification);

/**
 * @brief Close an XNotif server.
 * @param server Server object.
 * @return Error code.
 */
enum xnotif_error xnotif_server_close(struct xnotif_server *server);

// Client

/**
 * @brief Open an XNotif client.
 * @param client Client object.
 * @param display Display number. -1 to automatically determine the display.
 * @return Error code.
 */
enum xnotif_error xnotif_client_open(struct xnotif_client *client,
                                     int display);

/**
 * @brief Prepare and send a notification.
 * @param client Client object.
 * @param urgency Notification urgency.
 * @param title Notification title. NULL if empty.
 * @param body Notification body.
 * @return Error code.
 */
enum xnotif_error xnotif_client_send(struct xnotif_client *client,
                                     enum xnotif_urgency urgency,
                                     char *title,
                                     char *body);

/**
 * @brief Send a notification.
 * @param client Client object.
 * @param notification Notification object.
 * @return Error code.
 */
enum xnotif_error xnotif_client_send_raw(struct xnotif_client *client,
                                         struct xnotif_notification *notification);

/**
 * @brief Close an XNotif client.
 * @param client Client object.
 * @return Error code.
 */
enum xnotif_error xnotif_client_close(struct xnotif_client *client);
