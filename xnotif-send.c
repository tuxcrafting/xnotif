#include "libxnotif.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static void print_usage(int argc, char **argv) {
	printf("Usage: %s [-012h] [-d display] { <title> <body> } / { <body> }\n", argv[0]);
	printf("Send a XNotif notification on the current display.\n");
	printf("\t-0\tLow urgency.\n");
	printf("\t-1\tMedium urgency (default).\n");
	printf("\t-2\tHigh urgency.\n");
	printf("\t-d\tSpecify display number.\n");
	printf("\t-h\tPrint this help message.\n");
}

int main(int argc, char *argv[]) {
	enum xnotif_urgency urgency = XNOTIF_URG_MEDIUM;
	int display = -1;

	// parse options.
	int opt;
	while ((opt = getopt(argc, argv, "012dh")) != -1) {
		switch (opt) {
		case '0':
			urgency = XNOTIF_URG_LOW;
			break;
		case '1':
			urgency = XNOTIF_URG_MEDIUM;
			break;
		case '2':
			urgency = XNOTIF_URG_HIGH;
			break;
		case 'd':
			if (optind == argc) {
				print_usage(argc, argv);
				return 1;
			}
			display = atoi(argv[optind++]);
			break;
		default:
			print_usage(argc, argv);
			return 1;
		}
	}

	// get title and body.
	char *title, *body;
	switch (argc - optind) {
	case 1:
		title = NULL;
		body = argv[optind];
		break;
	case 2:
		title = argv[optind];
		body = argv[optind + 1];
		break;
	default:
		print_usage(argc, argv);
		return 1;
	}

	// send notification.
	struct xnotif_client client;
	enum xnotif_error err;
	if ((err = xnotif_client_open(&client, display)) != XNOTIF_SUCCESS) {
		fprintf(stderr, "xnotif_client_open() failed: %s\n", xnotif_strerr(err));
		return 1;
	}
	if ((err = xnotif_client_send(&client, urgency, title, body)) != XNOTIF_SUCCESS) {
		fprintf(stderr, "xnotif_client_send() failed: %s\n", xnotif_strerr(err));
		xnotif_client_close(&client);
		return 1;
	}
	xnotif_client_close(&client);

	return 0;
}
