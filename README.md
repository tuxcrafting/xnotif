# XNotif

XNotif is a simple protocol for X notifications, that I designed because I was pissed at how ridiculously complex the FreeDesktop-standard DBus notifications are.

There may be some bugs present, but I think it's pretty stable currently.

Included are:

* `libxnotif` - A very simple library for interfacing with XNotif.
* `xnotif-send` - A command line tool to send XNotif notifications, similar to `notify-send`.
* `xnotif-echoserver` - An example XNotif server that prints the received notifications.

See `spec.md` for a specification of the protocol.

## Bugs/Limitations

* Notification datagrams are limited to 64 kibibytes.
