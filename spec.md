# XNotif

XNotif is a simple standard for desktop notifications.

Notification datagrams are sent to the UDP Unix socket at `/tmp/xnotif-$DISPLAY.sock` where `$DISPLAY` is the X display number (identifier without the initial column and screen number).

The datagrams have this structure:

    struct xnotif_datagram_v1 {
        uint8_t version;
        uint8_t urgency;
        uint8_t title[];
        uint8_t body[];
    }

* `version` is always `1`.
* `urgency` is `0`, `1`, or `2`, corresponding to low, medium and high priority respectively.
* `title` is a NUL-terminated string of the title of the notification. It can be empty.
* `body` is a NUL-terminated string of the body of the notification. It can not be empty.
