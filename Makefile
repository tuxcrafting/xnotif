.POSIX:

CC=cc
CFLAGS=-fpic -Wall -Werror -O2
LDFLAGS=-shared
PREFIX=/usr/local
DESTDIR=
INSTALLDIR=$(DESTDIR)$(PREFIX)

all: xnotif-send xnotif-echoserver libxnotif.so libxnotif.a

xnotif-send: xnotif-send.c libxnotif.h libxnotif.a
	$(CC) $< libxnotif.a -o $@ $(CFLAGS)

xnotif-echoserver: xnotif-echoserver.c libxnotif.h libxnotif.a
	$(CC) $< libxnotif.a -o $@ $(CFLAGS)

libxnotif.o: libxnotif.c libxnotif.h
	$(CC) $< -o $@ -c $(CFLAGS)

libxnotif.so: libxnotif.o
	$(CC) $< -o $@ $(LDFLAGS)

libxnotif.a: libxnotif.o
	ar rcs $@ $<

clean:
	rm -f xnotif-send xnotif-echoserver libxnotif.so libxnotif.a libxnotif.o

install: xnotif-send libxnotif.so libxnotif.a
	mkdir -p $(INSTALLDIR)/bin $(INSTALLDIR)/lib $(INSTALLDIR)/include
	cp xnotif-send $(INSTALLDIR)/bin
	cp libxnotif.so libxnotif.a $(INSTALLDIR)/lib
	cp libxnotif.h $(INSTALLDIR)/include

uninstall:
	rm -f $(INSTALLDIR)/bin/xnotif-send $(INSTALLDIR)/lib/libxnotif.so $(INSTALLDIR)/lib/libxnotif.a $(INSTALLDIR)/include/libxnotif.h
